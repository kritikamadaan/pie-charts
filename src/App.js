import React from 'react';
import './App.css';
import PieCharts from './components/Piechartss';
import MinimalPieCharts from './components/Minimalpiechart';
import FunnelChart from './components/Charts/FunnelChart';
import PieChartss from './components/Charts/PieCharts';
import LabelledPie from './components/Charts/LabelledPie';
import Circularcharts from './components/Charts/Circularcharts';
import ColumnChart from './components/Charts/ColumnChart';
import Simplechart from './components/HighCharts/Simplechart';

function App() {
  return (
    <div className="App">
    <Simplechart/>
    <PieCharts/>
    <MinimalPieCharts/>
    <PieChartss/>
    <LabelledPie/>
    <Circularcharts/>
    <ColumnChart/>
    <FunnelChart/>
    </div>
  );
}

export default App;
