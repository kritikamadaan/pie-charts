import React from 'react'
import { render } from 'react-dom'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
 
 function Simplechart() {
    const options = {
        title: {
          text: 'My High chart'
        },
        series: [{
          data: [1, 2, 3]
        }]
      }
      return (
          <div>
          <HighchartsReact
          highcharts={Highcharts}
          options={options}
        /> 
          </div>
      )
  }
  
  export default Simplechart
  