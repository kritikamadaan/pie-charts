import { PieChart } from 'react-minimal-pie-chart';
import React from 'react'

function MinimalPieCharts() {
    const data =[
        { key:1,title: 'One', value: 10, color: '#E38627' },
        { key:2,title: 'Two', value: 15, color: '#C13C37' },
        { key:3,title: 'Three', value: 20, color: '#6A2135'},
        { key:4,title: 'Three', value: 20, color: '#aaaaaa'},
      ]
    return (
        <div style={{width:"30%",height:"30%"}}>
        <PieChart
        data={data}
        />
        </div>
    )
}

export default MinimalPieCharts
