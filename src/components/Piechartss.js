import PieChart from 'react-simple-pie-chart';
import React from 'react'

function PieCharts() {
const data =[
    {
      color: '#f00',
      value: 10,
    },
    {
      color: 'black',
      value: 20,
    },
    {
      color: 'yellow',
      value: 20,
    },
    
  ]
    return (
        <div style={{height:"30%",width:"30%"}}>
        <PieChart
        slices={data} />
        </div>
    )
}

export default PieCharts

